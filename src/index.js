import http from 'http'

const PORT = 8000;

type Message = string

export const helloWorld = (): Message => 'Hello World!'

http.createServer((req, res) => {
  const msg: Message = helloWorld()
  res.write(msg)
  res.end()
}).listen(PORT);

console.log(`Server listening at localhost:${PORT}`)
