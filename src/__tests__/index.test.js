import { helloWorld } from '~';

describe('Example App', () => {
  it('should write hello world on screen', () => {
    expect(helloWorld()).toBe('Hello World!');
  })
})
