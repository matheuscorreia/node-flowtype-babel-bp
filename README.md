Node babel flowtype minimalistic boilerplate :seedling:
--

 - Babel env preset
 - Flowtype
 - Dev enviroment including jest :black_joker:

### Start doing stuff
```bash
git clone https://gitlab.com/matheuscorreia/node-flowtype-babel-bp.git <dirname>
```

Make changes on `src/index.js`.

Run `yarn start` to start development enviroment.

Run `yarn test` to run tests

Good Hacking!
